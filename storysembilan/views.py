from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.models import User
from django.core.serializers import json
from django.http import HttpResponseBadRequest, JsonResponse
from django.shortcuts import render, redirect
import json

# Create your views here.
from django.views.decorators.csrf import csrf_exempt
app_name = 'storysembilan'

def storysembilan(request):
    if request.user.is_authenticated:
        context = {
            'user': request.user
        }
        return render(request, 'storysembilan.html', context)
    else:
        return redirect('/storysembilan/auth/')

def auth(request):
    return render(request, 'login.html')

def logout_user(request):
    logout(request)
    return redirect('/storysembilan/auth/')

@csrf_exempt
def api_login(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        if 'username' in data and 'password' in data:
            user = authenticate(request, username=data['username'], password=data['password'])
            if user is not None:
                login(request, user)
                return JsonResponse({
                    'status': 200,
                    'message': 'Successful login'
                })
            else:
                try:
                    User.objects.get(username=data['username'])
                    return JsonResponse({
                        'status': 401,
                        'message': 'Wrong Password'
                    })
                except User.DoesNotExist:
                    return JsonResponse({
                        'status': 401,
                        'message': 'Username does not exist'
                    })
    return HttpResponseBadRequest()

@csrf_exempt
def api_sign_up(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        if 'username' in data and 'password' in data and 'email' in data:
            try:
                User.objects.create_user(data['username'], data['email'], data['password'])
                return JsonResponse({
                    'status': 200,
                    'message': 'Your account is now ready to use'
                })
            except:
                return JsonResponse({
                    'status': 500,
                    'message': 'Username is invalid'
                })
    return HttpResponseBadRequest()