$(document).ready(function () {
    $('#createAccountLink').on('click', function () {
        $('#logIn').addClass('hide');
        $('#signUp').removeClass('hide');

        $('.error-message').css('display', 'none');
        $('.success-message').css('display', 'none');
    })

    $('#loginLink').on('click', function () {
        $('#logIn').removeClass('hide');
        $('#signUp').addClass('hide');

        $('.error-message').css('display', 'none');
        $('.success-message').css('display', 'none');
    })

    $('#logInButton').on('click', function () {
        var button = this;
        $.ajax({
            type: 'POST',
            url: '/story9/api/v1/login/',
            data: JSON.stringify({
                'username': $('input[name="username"]').val(),
                'password': $('input[name="password"]').val()
            }),
            contentType: 'application/json',
            beforeSend: function (jqXHR, settings) {
                $(button).children('div.lds-ring').removeClass('loading-hide');
                $(button).children('div.button-inner').addClass('loading-hide');

                // Hide any message
                $('.error-message').css('display', 'none');
                $('.success-message').css('display', 'none');
            },
            complete: function (jqXHR, textStatus) {
                $(button).children('div.lds-ring').addClass('loading-hide');
                $(button).children('div.button-inner').removeClass('loading-hide');
            },
            success: function (data, textStatus, jqXHR) {
                if (data.status == 200) {
                    window.location.replace("/story9");
                } else {
                    $('.error-message').html(data.message);
                    $('.error-message').css('display', 'inline-block');
                }
            }
        })
    })
    $('#signUpButton').on('click', function () {
        var button = this;
        $.ajax({
            type: 'POST',
            url: '/story9/api/v1/signup/',
            data: JSON.stringify({
                'username': $('input[name="s_username"]').val(),
                'email': $('input[name="s_email"]').val(),
                'password': $('input[name="s_password"]').val()
            }),
            contentType: 'application/json',
            beforeSend: function (jqXHR, settings) {
                $(button).children('div.lds-ring').removeClass('loading-hide');
                $(button).children('div.button-inner').addClass('loading-hide');

                // Hide any message
                $('.error-message').css('display', 'none');
                $('.success-message').css('display', 'none');
            },
            complete: function (jqXHR, textStatus) {
                $(button).children('div.lds-ring').addClass('loading-hide');
                $(button).children('div.button-inner').removeClass('loading-hide');
            },
            success: function (data, textStatus, jqXHR) {
                if (data.status == 200) {
                    // Show success message
                    $('.success-message').html(data.message);
                    $('.success-message').css('display', 'inline-block');

                    // Change display to login
                    $('#logIn').removeClass('hide');
                    $('#signUp').addClass('hide');

                    // Fill username login section with username when register
                    $('input[name="username"]').val($('input[name="s_username"]').val());
                } else {
                    $('.error-message').html(data.message);
                    $('.error-message').css('display', 'inline-block');
                }
            }
        })
    });

    $('input').keypress(function (e) {
        if (e.which == 13) {
            if ($('#logIn').hasClass('hide')) {
                $('#signUpButton').click()
            } else if ($('#signUp').hasClass('hide')) {
                $('#logInButton').click()
            }
            return false;    // Same as prevent default
        }
    });
})