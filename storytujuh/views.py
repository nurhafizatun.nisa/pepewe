from django.shortcuts import render

# Create your views here.
def index(request): 
    context = {
        "accordion": [{
            "id": "target1",
            "header": "Current Activities",
            "body": "PMB Fasilkom UI 2020, Siwak 2020, Betis 2021"
        }, 
        {
            "id": "target2",
            "header": "Experience",
            "body": "Perak 2020, PMB Fasilkom UI 2020, Siwak 2020, Betis 2021"
        }, 
        {
            "id": "target3",
            "header": "Achievements",
            "body": "Soon :)"
        }, 
        {
            "id": "target4",
            "header": "Hobby",
            "body": "Watching Korean drama"
        }, ]
    }
    return render(request, 'index.html', context)
