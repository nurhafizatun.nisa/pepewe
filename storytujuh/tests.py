from django.test import TestCase,Client
from django.urls import resolve
from .views import index

# Create your tests here.
class Testing(TestCase):
    def test_landing_page_url_is_exist(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

    def test_storytujuh_using_to_do_list_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
