from django.urls import path
from . import views

app_name = 'storytujuh'

urlpatterns = [
    path('', views.index, name='storytujuh'),
]