from django.shortcuts import render
from django.http import JsonResponse
import requests as res
import json

# Create your views here.
app_name = 'storydelapan'

def books(request):
    return render(request, 'books.html')

def search_books_API(request):
    q = request.GET['q']
    url = "https://www.googleapis.com/books/v1/volumes?q=" + q
    obj = res.get(url)
    data = json.loads(obj.content)
    return JsonResponse(data, safe = False)