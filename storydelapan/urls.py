from django.conf.urls import url
from django.urls import path
from .views import *
from . import views

app_name = 'storydelapan'

urlpatterns = [
    path('', views.books, name='storydelapan'),
    path('data/', views.search_books_API, name='search_books_API'),
]